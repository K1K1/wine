The Wine development release 7.20 is now available.

What's new in this release:
  - Mono engine updated to version 7.4.0.
  - Font linking improvements.
  - A number of fixes for exception unwinding.
  - Support for dumping EMF spool files in WineDump.
  - Various bug fixes.

The source is available at:

  https://dl.winehq.org/wine/source/7.x/wine-7.20.tar.xz

Binary packages for various distributions will be available from:

  https://www.winehq.org/download

You will find documentation on https://www.winehq.org/documentation

You can also get the current source directly from the git
repository. Check https://www.winehq.org/git for details.

Wine is available thanks to the work of many people. See the file
AUTHORS in the distribution for the complete list.

----------------------------------------------------------------

Bugs fixed in 7.20 (total 29):

 - #15679  cygwin symlinks not working in wine
 - #29998  Crysis 2 (>1.0) huge graphical issues after loading a savegame
 - #38594  dbghelp_msc:pe_load_debug_directory Got a page fault while loading symbols (Visual C++ 2013 .pdb's)
 - #38604  winedbg: internal crash/hangs when running ieframe/tests/ie.c with pdb debug symbols
 - #44814  cygwin's bsdtar.exe needs a better ntdll.NtQueryEaFile stub
 - #46822  Edit control in ADL search dialog gets initially not drawn in DC++ 0.868, regression
 - #47595  Tequila City of Heroes Launcher - expect_no_runtimes Process exited with a Mono runtime loaded.
 - #48197  Autonauts in steam under wine has a memory leak.
 - #50573  division by zero in wineconsole when hovering mouse over
 - #51529  CloseHandle(INVALID_HANDLE_VALUE) returns FALSE, GetLastError is ERROR_INVALID_HANDLE
 - #51775  Axiom Verge 2 crashes on startup due to incomplete sapi implementation
 - #52545  ACE rights parser should allow octal and decimal formats
 - #52648  conhost.exe:tty test_read_console_control() fails in Hindi and Portuguese
 - #52790  winedbg 'bt all' filled with dbghelp_dwarf RULE_VAL_EXPRESSION fixmes
 - #53437  ZOSI Cloud crashes on start
 - #53455  shell32:recyclebin - test_query_recyclebin() gets unexpected recyclebin size in Wine
 - #53521  imm32:imm32 - test_default_ime_window_creation() fails on Windows 10 21H1+
 - #53552  Line width calculation failure in WineMono.Tests.System.Windows.Media.TextFormatting.TextFormatterTest.NewlineCharacterCollapsibleTest
 - #53608  Visual Novel Shin Koihime † Eiyuutan 4 plays opening movie in separate window
 - #53669  Problems with two separate MetaTrader4 programs on the same Wine desktop
 - #53679  The 64-bit ntdll:env fails on Windows 7
 - #53724  Obduction fails in a random way out of a handful of possible ways
 - #53770  Wrong locale data for Chinese locales for LOCALE_SNATIVEDIGITS class
 - #53800  CRYPT_GetBuiltinDecoder Unsupported decoder for lpszStructType 1.3.6.1.4.1.311.2.1.4
 - #53813  Let's encrypt certificate validation fails
 - #53821  winbase.h: ReOpenFile declaration missing (and not available in any other header)
 - #53829  Font links for MingLiU broken 32-bit wineprefixes
 - #53832  wbemprox: where-clause seems to be case-sensitive
 - #53838  Non-PE builds fail after "makedep: Make the installation directories architecture-generic."

----------------------------------------------------------------

Changes since 7.19:

Akihiro Sagawa (8):
      quartz/tests: Add a background brush test for video renderer.
      quartz: Fix video flickering.
      shell32/tests: Add DBCS file name tests for DragQueryFile.
      shell32/tests: Add more DragQueryFile tests.
      shell32: Merge ANSI part of DROPFILES handler into DragQueryFileW.
      shell32: Reimplement DragQueryFileA to rely on its Unicode version.
      shell32: Returns a number of copied characters in DropQueryFileW.
      shell32: Returns a number of copied bytes in DropQueryFileA.

Alex Henrie (14):
      include: Add TOOLINFO[AW].
      shell32: Move strndupW to dde.c.
      shell32: Use standard C functions for memory allocation in dde.c.
      shell32: Introduce combine_path helper for DDE.
      shell32: Sanitize Program Manager icon and group names.
      include: Add WSANO_ADDRESS to winsock2.h.
      include: Move strto[iu]max(_l) from stdlib.h to inttypes.h.
      uuid: Add devguid.h.
      msvcrt: Change return type of _putenv_s to errno_t.
      include: Add _putenv_s.
      shell32: Don't use strdupW or heap_* functions in shellole.c.
      shell32: Don't use strdupW or heap_* functions in shelldispatch.c.
      shell32: Don't use strdupW or heap_* functions in shelllink.c.
      shell32: Remove unused function strdupW.

Alexandre Julliard (21):
      nls: Update locales that have been added in recent Windows versions.
      nls: Allow specifying a locale's native digits.
      makedep: Pass a generic architecture value instead of the is_cross flag to various helpers.
      makedep: Make the target flags architecture-generic.
      makedep: Make the debug flags architecture-generic.
      makedep: Make the delay load flags architecture-generic.
      makedep: Make the extra C flags architecture-generic.
      makedep: Make the installation directories architecture-generic.
      makedep: Make the object files architecture-generic.
      makedep: Make the import library files architecture-generic.
      makedep: Make the makefile targets architecture-generic.
      makedep: Add support for architecture-specific sources.
      makedep: Add support for architecture-specific IDL objects.
      makedep: Make the resource files architecture-generic.
      makedep: Add a helper function to build a source file for one architecture.
      makedep: Add a helper function to get a cross-compilation make variable.
      makedep: Add a helper function to build an arch-specific module name.
      makedep: Make the disabled flag architecture-generic.
      makefiles: Store PE objects in subdirectories in the build tree.
      configure: Remove no longer needed check for dlltool.
      makedep: Remove arch directories on distclean.

Alistair Leslie-Hughes (8):
      crypt32: Only report Unimplemented decoder when not found in external dll.
      sapi: Implement ISpObjectToken OpenKey.
      sapi: Move token structure before EnumTokens function.
      sapi: Store registry key to Enum builder.
      sapi: Implement ISpObjectTokenEnumBuilder Item.
      sapi: Implement ISpObjectTokenEnumBuilder Next.
      sapi: Add default voice registry key.
      sapi: Add Voice enum tests.

Anton Baskanov (6):
      quartz/tests: Add tests for MPEG layer-3 decoder interfaces.
      quartz/tests: Add tests for MPEG layer-3 decoder aggregation.
      quartz/tests: Test MPEG layer-3 decoder unconnected filter state.
      quartz/tests: Add tests for IBaseFilter_EnumPins() on MPEG layer-3 decoder.
      quartz/tests: Add tests for IBaseFilter_FindPin() on MPEG layer-3 decoder.
      quartz/tests: Add tests for querying MPEG layer-3 decoder pin info.

Bartosz Kosiorek (26):
      msvcrt: Add _mbctolower_l partial implementation.
      msvcrt: Add _mbctoupper_l partial implementation.
      msvcrt: Add _mbslwr_s_l partial implementation.
      msvcrt: Add _mbsupr_s_l partial implementation.
      msvcrt: Add _mbschr_l implementation.
      msvcrt: Add _mbsicmp_l implementation.
      msvcrt: Add _mbclen_l implementation.
      msvcrt: Fix error handling for _mbscmp_l.
      msvcrt: Add _ismbslead_l implementation.
      msvcrt: Add _ismbstrail_l implementation.
      msvcrt: Add _ismbcsymbol_l implementation.
      msvcrt: Add _mbsinc_l implementation.
      msvcrt: Fix error handling for strcat_s.
      msvcrt: Fix error handling for strcpy_s.
      gdiplus: Add GdipSetPenCompoundArray implementation.
      gdiplus: Add GdipGetPenCompoundCount implementation.
      gdiplus: Add GdipGetPenCompoundArray implementation.
      msvcrt: Add _ismbchira_l implementation.
      msvcrt: Add _ismbckata_l implementation.
      msvcrt: Add _mbbtombc_l implementation.
      ucrtbase: Export more _o_* functions.
      msvcrt: Add _mbcjistojms_l implementation.
      msvcrt: Add _mbcjmstojis_l implementation.
      msvcrt: Add _mbctombb_l implementation.
      msvcrt: Add _mbctohira_l implementation.
      msvcrt: Add _mbctokata_l implementation.

Brendan Shanks (5):
      mmdevapi: Set the name of internal threads.
      xinput1_3: Set thread name for hid_update_thread_proc.
      sechost: Set the name of internal threads.
      ntdll: Set native thread names on Linux when set with SetThreadDescription().
      server: Only require THREAD_SET_LIMITED_INFORMATION access to set thread description.

Chilung Chan (1):
      po: Update Traditional Chinese translation.

Connor McAdams (15):
      uiautomationcore: Add support for multiple providers on a single HUIANODE.
      uiautomationcore: Determine provider type for nested node providers.
      uiautomationcore: Add support for getting HWND providers to UiaNodeFromProvider().
      uiautomationcore: Add support for getting HWND providers to uia_node_from_lresult().
      uiautomationcore/tests: Ignore UIA_NativeWindowHandlePropertyId in certain tests.
      uiautomationcore: Implement UiaRegisterProviderCallback.
      uiautomationcore: Add UiaGetUpdatedCache stub.
      uiautomationcore: Implement UiaGetUpdatedCache.
      uiautomationcore: Add basic UiaCondition support to UiaGetUpdatedCache.
      uiautomationcore/tests: Add tests for boolean UIA ConditionTypes.
      uiautomationcore/tests: Add ConditionType_Property tests.
      uiautomationcore: Add support for ConditionType_Not conditions.
      uiautomationcore: Add support for ConditionType_{And/Or} conditions.
      uiautomationcore: Implement ConditionType_Property conditional for UIAutomationType_Bool properties.
      uiautomationcore: Add support for UIAutomationType_IntArray property comparisons.

Daniel Lehman (4):
      kernel32/tests: Add tests for CancelSynchronousIo.
      ntdll: Add NtCancelSynchronousIoFile stub.
      ntdll: Implement NtCancelSynchronousIoFile.
      kernelbase: Call NtCancelSynchronousIoFile in CancelSynchronousIo.

David Kahurani (1):
      xmllite/writer: Null terminate duplicated strings.

Eric Pouech (11):
      dbghelp: Rework dwarf2_get_ranges() helper.
      dbghelp: No longer call read_range() for inline site (dwarf).
      dbghelp: Allow symt_block to be defined over non contiguous chunks.
      dbghelp: Silence some FIXMEs.
      oleaut32/tests: Fix test failing on UTF-8 locale.
      ntdll/tests: Force alignment of output structures.
      dbghelp: Use heap functions for allocation.
      dbghelp: Realloc array of buckets inside struct vector.
      conhost/tests: Fix tests for some keyboard layouts.
      conhost/tests: Fix some tests on input sequences.
      kernel32/tests: Workaround some broken Windows behavior in console tests.

Esme Povirk (3):
      user32: Test Get/SetWindowPlacement with invalid length.
      win32u: Reject invalid length in SetWindowPlacement.
      mscoree: Update Wine Mono to 7.4.0.

Etaash Mathamsetty (3):
      ntoskrnl.exe: Implement IoCreateNotificationEvent.
      ntoskrnl.exe: Fix formatting in IoCreateSynchronizationEvent.
      ntoskrnl.exe: Use LoadLibraryW for MmGetSystemRoutineAddress.

Fabian Maurer (1):
      ntdll: Prevent double free (Coverity).

Florian Kübler (1):
      ntdll: Add CFI unwind info to __wine_syscall_dispatcher (x86_64).

François Gouget (8):
      wintrust: Add a trailing linefeed to an ERR() message.
      winetest: Let the get_subtests() caller report errors.
      dbghelp: Fix the trailing linefeed of a WARN() message.
      d3d10core/tests: Fix the spelling of a comment.
      dbghelp: Fix the spelling of a couple of comments.
      rpcrt4/tests: Fix the spelling of a couple of comments.
      msdasql: Fix a typo in the is_fixed_length() function name.
      ddraw/tests: Mark tests failing randomly on Windows 8 as flaky.

Gabriel Ivăncescu (28):
      jscript: Implement Function.prototype.bind's `thisArg` properly.
      jscript/tests: Fix copy paste mistake in JSON test.
      jscript: Implement `reviver` argument for JSON.parse.
      wininet: Handle offline state in InternetGetConnectedStateExW.
      netprofm: Handle non-internet connectivity.
      mshtml: Move the ConnectionPointContainer out of basedoc.
      mshtml: Move the IServiceProvider interface out of basedoc.
      mshtml: Move the Persist interfaces out of basedoc.
      mshtml: Move the IOleCommandTarget interface out of basedoc.
      mshtml: Move the OleObj interfaces out of basedoc.
      mshtml: Move HTMLDocumentObj implementation to oleobj.c.
      mshtml: Move the remaining non-IHTMLDocument* interfaces out of basedoc.
      mshtml: Move the IHTMLDocument2 interface out of basedoc.
      mshtml: Move the IHTMLDocument3 interface out of basedoc.
      mshtml: Move the IHTMLDocument4 interface out of basedoc.
      mshtml: Move the IHTMLDocument5 interface out of basedoc.
      mshtml: Move the IHTMLDocument6 interface out of basedoc.
      mshtml: Move the IHTMLDocument7 interface out of basedoc.
      mshtml: Move the IDispatchEx interface out of basedoc.
      mshtml: Get rid of the outer_unk in basedoc.
      mshtml: Get rid of the outer window in basedoc.
      mshtml: Get rid of the HTMLDocument basedoc.
      jscript: Implement VariantChangeType for VT_DISPATCH and VT_UNKNOWN.
      mshtml: Allow null or undefined listeners in attachEvent/detachEvent.
      mshtml: Allow null or undefined func in addEventListener.
      mshtml: Get rid of a EVENTID_LAST special case.
      mshtml: Implement document.mimeType.
      mshtml: Implement document.referrer.

Gerald Pfeifer (2):
      ntdll: Fix build on systems without ENODATA.
      loader: Unbreak FreeBSD builds (ARRAY_SIZE undefined).

Hans Leidekker (2):
      crypt32: Parse OCSP responder name.
      wbemprox: WQL string comparisons are case insensitive.

Hugh McMaster (4):
      kernel32/tests: Remove tests comparing expected output in certain functions.
      kernelbase: Add stubs for GetConsoleOriginalTitleA/W().
      kernel32/tests: Add tests for GetConsoleOriginalTitleA/W().
      kernelbase: Avoid memory leaks in GetConsoleTitleW().

Jacek Caban (2):
      user.exe: Pass resource ID as a string in DIALOG_CreateControls16.
      makedep: Always use -mno-cygwin for extra test modules.

Jinoh Kang (8):
      win32u: Fix data race in NtUserGetProcessDpiAwarenessContext.
      gdiplus: Avoid recursively locking image in GdipImageRotateFlip.
      gdiplus: Avoid copying GpImage's busy flag in select_frame_wic().
      gdiplus: Replace GpImage's busy flag with SRWLOCK.
      user32/tests: Test for window exposure behaviours.
      kernel32/tests: Add test for pipe name with a trailing backslash.
      ntdll/tests: Add tests for pipe names.
      ntdll/tests: Add more tests for \Device\NamedPipe and \Device\NamedPipe\.

Joel Holdsworth (2):
      ntdll/tests: Add initial tests for reparse points.
      ntdll: Add FSCTL_GET_REPARSE_POINT semi-stub.

Martin Storsjö (4):
      win32u: Make sure that the stack buffer in set_multi_value_key is large enough.
      ntdll: Move the dwarf reading routines to a shareable header.
      ntdll: Add support for aarch64 in the dwarf implementation.
      ntdll: Use the local dwarf implementation on arm64.

Michael Stefaniuc (11):
      ntdll: Use ARRAY_SIZE instead of open coding it.
      riched20/tests: Use ARRAY_SIZE instead of open coding it.
      kernel32/tests: Use ARRAY_SIZE instead of open coding it.
      server: Use ARRAY_SIZE instead of open coding it.
      loader: Use ARRAY_SIZE instead of open coding it.
      libs/wine: Use ARRAY_SIZE instead of open coding it.
      widl: Use ARRAY_SIZE instead of open coding it.
      winegcc: Use ARRAY_SIZE instead of open coding it.
      wmc: Use ARRAY_SIZE instead of open coding it.
      wrc: Use ARRAY_SIZE instead of open coding it.
      makedep: Use ARRAY_SIZE instead of open coding it.

Mihail Ivanchev (1):
      include: Add prototype for ReOpenFile().

Nikolay Sivov (13):
      dwrite/tests: Use existing to create factory instance.
      d2d1: Test device context type in BindDC().
      ntdll: Add some already implemented security descriptor exports.
      kernelbase: Forward some of the security descriptor functions to ntdll.
      kernelbase: Forward some directory info classes in GetFileInformationByHandleEx().
      kernelbase: Make sure to provide a message for missing information classes in GetFileInformationByHandleEx().
      kernelbase: Forward some more classes in GetFileInformationByHandleEx().
      ntdll: Partially implement MemoryRegionInformation query.
      kernelbase: Add QueryVirtualMemoryInformation().
      user32: Add DragObject() stub.
      win32u: Move NtUserDragDetect() to window.c.
      mf/samplegrabber: Send MEStreamSinkRateChanged event.
      mf/samplegrabber: Send MEStreamSinkScrubSampleComplete event.

Paul Gofman (14):
      wbemprox: Fix string length in get_value_bstr().
      msvcrt: Display message box in abort() for specific CRT versions only.
      winmm: Use a global timeout for refreshing joystick devices.
      win32u: Store GPU luid in adapters cache.
      win32u: Move NtUserDisplayConfigGetDeviceInfo implementation from user32.
      win32u: Store output id in monitors cache.
      win32u: Implement NtUserDisplayConfigGetDeviceInfo(DISPLAYCONFIG_DEVICE_INFO_GET_TARGET_NAME).
      ws2_32/tests: Make test_so_reuseaddr() more conclusive.
      ws2_32/tests: Add tests for reusing address without SO_REUSEADDR.
      ws2_32/tests: Also test TCP6 in test_so_reuseaddr().
      ntdll: Move SO_REUSEADDR handling to server.
      server: Track SO_REUSEADDR value.
      server: Set Unix SO_REUSEADDR on all the TCP sockets.
      ws2_32/tests: Also test bind to any together with loopback in a different order.

Piotr Caban (34):
      winedump: Add initial EMF spool files support.
      winedump: Fix buffer overflow in debugstr_wn helper.
      winedump: Don't use static offset in dump_emfrecord.
      winedump: Dump metafiles in EMF spool files.
      winedump: Add prefix when dumping EMF files.
      winedump: Add support for EMRI_METAFILE_EXT records.
      comctl32: Add helper for calling PROPERTYSHEETPAGE callback.
      comctl32: Add helper for getting flags from HPROPSHEETPAGE.
      comctl32: Add helper for loading dialog template from HPROPSHEETPAGE.
      comctl32: Add helper for getting title from HPROPSHEETPAGE.
      comctl32: Add helper for getting icon from HPROPSHEETPAGE.
      comctl32: Add helper for creating page HWND from HPROPSHEETPAGE.
      comctl32: Add helper for getting template from HPROPSHEETPAGE.
      comctl32: Add helper for setting header title in HPROPSHEETPAGE.
      comctl32: Add helper for setting header subtitle in HPROPSHEETPAGE.
      comctl32: Add helper for drawing header title and subtitle obtained from HPROPSHEETPAGE.
      msvcrt: Fix mbcasemap initialization.
      msvcrt: Add support for multi-byte characters in _mbctolower_l.
      msvcrt: Add support for multi-byte characters in _mbctoupper_l.
      msvcrt: Fix error handling in _mbsupr_s_l.
      msvcrt: Fix error handling in _mbslwr_s_l.
      winedump: Handle NULL in get_unicode_str.
      msvcrt: Fix _putenv_s return value on error.
      msvcrt: Fix _wputenv_s return value on error.
      msvcrt: Fix _wputenv_s invalid argument handling.
      comctl32: Create page if PROPSHEETPAGE was passed as HPROPSHEETPAGE to PropertySheetAW or PSM_INSERTPAGE.
      comctl32: Don't use PROPSHEETPAGE structure to store internal HPROPSHEETPAGE data.
      comctl32: Fix PROPSHEETPAGE[AW] structure content in messages and callbacks.
      comctl32: Add support for adding extra data in CreatePropertySheetPage.
      compstui: Add CPSFUNC_ADD_PFNPROPSHEETUI support.
      compstui: Add CPSFUNC_ADD_PROPSHEETPAGE support.
      compstui: Test that callbacks are executed.
      maintainers: Update Microsoft C Runtime files list.
      winemac.drv: Fix registry path size in create_original_display_mode_descriptor.

Rémi Bernon (10):
      opengl32: Split is_extension_supported helper.
      opengl32: Build extension list in is_extension_supported.
      opengl32: Move filter_extensions (et al.) around.
      opengl32: Use has_extension in filter_extensions_index.
      win32u: Use session BaseNamedObjects for display_device_init mutex.
      winex11.drv: Use session BaseNamedObjects for display_device_init mutex.
      dinput/tests: Add dummy flaky test count.
      ntoskrnl.exe/tests: Add dummy flaky test count.
      schedsvc/tests: Add dummy flaky test count.
      win32u: Write display modes to registry as binary blobs.

Stanislav Motylkov (1):
      hhctrl.ocx: Fix toolbar icons order.

Vladislav Timonin (2):
      d2d1: Update to ID2D1Device1.
      d2d1: Implement ID2D1Device1::CreateDeviceContext.

Zebediah Figura (8):
      wined3d: Do not enforce GL map access for resources with WINED3D_RESOURCE_ACCESS_CPU.
      ws2_32/tests: Add more tests for iosb contents while a recv is pending.
      ntdll: The async handle passed to set_async_direct_result() cannot be NULL.
      ntdll: Fill the IOSB in sock_recv() only inside the "if (alerted)" block.
      ntdll: Combine the "if (alerted)" blocks in sock_recv().
      ntdll: Fill the IOSB in sock_send() only inside the "if (alerted)" block.
      ntdll: Combine the "if (alerted)" blocks in sock_send().
      ntdll: Fill the IOSB in sock_transmit() only inside the "if (alerted)" block.

Zhiyi Zhang (10):
      comctl32/tests: Ignore WM_NCPAINT when testing WM_THEMECHANGED for updown control.
      comctl32/tests: Test if WM_STYLECHANGED repaints controls.
      comctl32/treeview: Don't repaint when handling WM_STYLECHANGED.
      comctl32/listview: Don't repaint when handling WM_STYLECHANGED.
      comctl32/animate: Don't repaint when handling WM_STYLECHANGED.
      comctl32/datetime: Don't repaint when handling WM_STYLECHANGED.
      comctl32/syslink: Don't repaint when handling WM_STYLECHANGED.
      comctl32/tab: Don't repaint when handling WM_STYLECHANGED.
      comctl32/updown: Don't repaint when handling WM_STYLECHANGED.
      comctl32/tests: Use a toolbar with TBSTYLE_FLAT in toolbar visual tests.

Ziqing Hui (10):
      win32u: Add mechanism for font specific system links.
      win32u: Add font link for MS UI Gothic.
      win32u: Add more linked fonts for Tahoma.
      win32u: Add locale_dependent member to struct system_link_reg.
      win32u: Add font links for Microsoft JhengHei.
      win32u: Add font links for MingLiU.
      win32u: Add font links for MS Gothic.
      win32u: Add font links for Yu Gothic UI.
      win32u: Add font links for Meiryo.
      win32u: Add font links for MS Mincho.
